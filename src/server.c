#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>

#include <sqlite3.h>

#include "dbg.h"

#define HTML_LENGTH 5 // ".html" length
#define SHA512_HASH_LENGTH 128 // since this is sha512 with encoding

int print_file(char *, char *);
int login(char[], int);
void print_substring(char *, char *);
int print_with_content_type(char *);

// these will be used all over the place, so they are initialized here.
sqlite3 *db;
sqlite3_stmt *res;
int sqlite3_rc;

int 
main(void)
{
	char *path = getenv("PATH_INFO");
	check(path, "Path cannot be null");

	int path_len = strlen(path); 
	// now initialize the database
	sqlite3_rc = sqlite3_open(DB_DIR, &db);
	check(sqlite3_rc == SQLITE_OK, "Unable to open the database. %s", sqlite3_errmsg(db));

	// these are static content exceptions
	if (!strncmp(path, "/", 2)) {
		check(print_file("index.html", "text/html") == 0, "Unable to print index.html.");
	} else if (!strncmp(path, "/dashboard", 10)) {
		puts("Status: 301\nLocation: /login\n\n");

	} else if (!strncmp(path, "/api", 4)) {
		char *content_length_str = getenv("CONTENT_LENGTH");
		check(content_length_str, "Message required for path /api");
		int content_length;
#ifdef __OpenBSD__
#include <limits.h>
		const char *errstr; 
		content_length = strtonum(content_length_str, LLONG_MIN, LLONG_MAX, &errstr) + 1;
		check(errstr == NULL, "Error in parsing content length %s: %s ", content_length_str, errstr); 
#else
		content_length = strtol(content_length_str, NULL, strlen(content_length_str)) + 1; // untested
		check(content_length, "Error in parsing int from str");
#endif
		char content[content_length + 1];
		content[content_length] = '\0';

		check(fread(content, 1, content_length, stdin), "Error reading stdin");	// fgets() will  not continue after '\n'

		// this is just for testing purposes.
		if (strncmp(content, "sql", 3)) {
			check(login(content, content_length) == 0, "Login failed.");	
		} else {
			printf("Content-type: text/html\n\nsqlite3 version: %s", sqlite3_libversion());
		}		
	} else {
		// this is where static content goes. if no file extension is specified, html is assumed.
		if (strchr(path, '.') == NULL) {
			char buf[path_len + HTML_LENGTH]; // removing the "/" in front of the path so we dont need an extra char for the null byte
			strncpy(buf, path + 1, path_len); // path + 1 = the index after "/"
			strncat(buf, ".html", HTML_LENGTH); // find the related html file
		
			if (print_file(buf, "text/html")) 
				puts("Status: 404\n\n"
					"<title>Not Found</title>"
					"<h1>404 Not Found</h1>");
		} else {
			check(print_with_content_type(path) == 0, "Error occured while printing %s", path);
		}
	}

	return 0;
error:
	// error handling is becoming a mess, so eventually we will make a different function for this
	printf("Status: 400\n\nError: %s", clean_errno());
	sqlite3_close(db);
	return 1; // need to be able to differentiate from client and server errors
}

int
print_file(char *name, char *content_type)
{
	if (name == NULL && content_type == NULL) 
		return 1; // check() didnt work due to bypassing initilization error for variable size arrays

	FILE *file;
	int file_length, name_length = strlen(name); // name_length was created to save instructions

	// get length
	struct stat st;

	// concat the strings together to form an absolute path
	char filename[FILE_DIR_LEN + name_length + 2];
	strncpy(filename, FILE_DIR, FILE_DIR_LEN);
	strncat(filename, "/", 1);
	strncat(filename, name, name_length);

	// get the length of the file
	stat(filename, &st);
	file_length = st.st_size;

	// allocate space to store the data
	char data[file_length + 1]; 

	file = fopen(filename, "r"); 
	check(file, "Unable to open %s", name);

	// read the data into the buffer and print the data in the buffer
	fread(&data, 1, file_length, file);

	printf("Content-Type: %s\n\n", content_type);
	
	// print it manually because it may contain null bytes
	for (int i = 0; i < file_length; i++)
		putchar(data[i]);
	
	return 0;
error:
	return 1;
}

int
login(char content[], int length) {
	char *username = malloc(length), *password = malloc(length), *index; // index will be used to hold the value of strchr() and as a temp val
	check(content && length, "Content and length can't be null");
	check_mem(username && password);
	
	index = strstr(content, "username=");
	check(index, "Username can't be null... Malformed request.");
	strcpy(username, index + 9); // to remove the "username=" at the beginning

	index = strstr(content, "password=");
	check(index, "Password can't be null... Malformed request.");
	strcpy(password, index + 9); // to remove the "password=" at the beginning

	// there will be some weird parsing, the data may include the next key/value. this resolves it.
	*((index = strchr(username, '&')) != 0 ? index : username + strlen(username) - 1) = '\0';
	*((index = strchr(password, '&')) != 0 ? index : password + strlen(password) - 1) = '\0';

	// now that the username and password has been parsed, query the database.
	sqlite3_rc = sqlite3_prepare_v2(db, "SELECT password FROM members WHERE user = ?;", -1, &res, 0);
	check(sqlite3_rc == SQLITE_OK, "Failed to obtain password for %s. %s", username, sqlite3_errmsg(db));
	
	sqlite3_rc = sqlite3_bind_text(res, 1, username, -1, 0);
	check(sqlite3_rc == SQLITE_OK, "Failed to bind username to compiled sql statement. %s", sqlite3_errmsg(db));

	sqlite3_rc = sqlite3_step(res);
	check(sqlite3_rc == SQLITE_ROW, "Login failed: user does not exist");
	
	check(!strncmp(sqlite3_column_text(res, 0), password, SHA512_HASH_LENGTH), "Password does not match.");

	puts("Content-type: text/html\n\nvalid login", password);

	free(username);
	free(password);
	return 0;
error:
	free(username);
	free(password);
	return 1;
}

void
print_substring(char *string, char *lastindex) {
	check(string < lastindex, "The last index has to be greater than the starting index.");

	// gets to here for the username, but not the password??
	for (int i = 0; i < lastindex - string; i++) 
		putchar(string[i]);
error:
	return;
}

int
print_with_content_type(char *path) {
	char *dot = strrchr(path, '.') + 1;
	int printSuccess;
		
	if (!strncmp(dot, "html", 4)) 
		printSuccess = print_file(path, "text/html");
	else if (!strncmp(dot, "css", 3)) 
		printSuccess = print_file(path, "text/css");
	else if (!strncmp(dot, "js", 2)) 
		printSuccess = print_file(path, "text/javascript");
	else if (!strncmp(dot, "json", 4)) 
		printSuccess = print_file(path, "application/json");
	else if (!strncmp(dot, "gif", 3)) 
		printSuccess = print_file(path, "image/gif");
	else if (!strncmp(dot, "png", 3)) 
		printSuccess = print_file(path, "image/png");
	else if (!strncmp(dot, "svg", 3))
		printSuccess = print_file(path, "image/svg+xml; charset=utf-8");
	else
		printSuccess = print_file(path, "application/octet-stream; charset=utf-8");
	
	check(printSuccess == 0, "Unable to print %s", path);
	return 0;
error:
	return 1;
}
