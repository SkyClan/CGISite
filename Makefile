CFLAGS=-g -Wall -DNDEBUG -Wextra -static -DFILE_DIR="\"/htdocs/skyclan.tk\"" -DFILE_DIR_LEN=19 -DDB_DIR="\"/skyclan.db\"" -DDB_DIR_LEN=11
LDLIBS=-I /usr/local/include -L /usr/local/lib -pthread -lsqlite3 -lm
SRCS!=ls src/*.c
SERVER_BIN=bin/skyclan.cgi

SERVER_BIN_LOCATION=/var/www/cgi-bin/
RESTART_CMD=doas slowcgi restart

all: 
	cc $(CFLAGS) -o $(SERVER_BIN) $(SRCS) $(LDLIBS)
	doas cp $(SERVER_BIN) $(SERVER_BIN_LOCATION)
	$(RESTART_CMD)
	

