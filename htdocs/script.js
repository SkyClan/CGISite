document.getElementById("loginbtn").addEventListener("click", () => {
  const formData = new FormData();
  formData.append("username", document.getElementById("username").value);
  formData.append("password", SHA512(document.getElementById("password").value));

  fetch("/api", 
	{
	  body: new URLSearchParams(formData),
	  method: "post"
	})
  .then((response) => response.text())
  .then((data) => document.write(data));
  window.history.pushState('dashboard', 'Dashboard', '/dashboard');
  // handing of the remember me will happen using sessionStorage/cookies 
});

/*
if (window.WebSocket) {
	const socket = new WebSocket("wss://skyclan.ml:8888/");
	
	socket.onopen = () => {
		socket.send("ur mom!");
		alert("Socket opened!");
	};

	socket.onmessage = (event) => {
		alert(event.data);
	};

	socket.onerror = (error) => {
		alert(`Error: ${error.message}`);
	};
}

	socket.onclose = (event) => {
		alert("Connection to server closed");
	};
} else {
	alert("Your browser does not support WebSocket.");
	// ideally use long polling in this case.
}
*/
