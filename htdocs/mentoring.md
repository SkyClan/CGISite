# mentoring
## What is it?
This is the process which is going to be undertaken by the apprentice so that he can become a warrior.

## Why is this needed?
In order to be able to make valid decisions with all the power given to them, one must understand what has already been done before and not to repeat it. This is the same reason why history is thought.

## How this is going to work
This is a simple process in which all the apprentice does is learn what happened in the past. There is no test because tests are pretty annoying anyways and we arent trying to be a school lol

## What you are going to do as a mentor
You are going to teach the following topics:
- The books (who what where why when how)
- Founded, the leaders, when it actually existed, what was it called originally
- What was YeetClan and The Resistance
- Why is there a site why there wasn't one in the past
- Who were og people
- What was the 
 - Coronovirus Act
 - Dawn of skyclan
 - Skybucks
- why discord
- understand current issues and what is being done to solve them.

## In order to accomplish the above...
- look at some old sites
- talk to people if you dont know
- ask spikestar 


