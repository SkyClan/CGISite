skyclan cgi site
----------------------

(O_O)

## What is this?
a site running via cgi, except for websockets which is used to handle messages.

## What language?
C, we all know and love C.

# Libs
- slowcgi (or any cgi server)

# How do you install it?
First, git clone this repo. Go into the makefile and change the directories of the macros "FILE_DIR" and "DB_DIR", and also their corresponding lengths. 

run make and you will have your compiled binaries in `SERVER_BIN_LOCATION` (as per the makefile).  

the database layout (you can set this up using sqlite3):
| User     | Rank | Address          | Apprentice | Site             | Comments      |
|----------|------|------------------|------------|------------------|---------------|
| Firestar | 1    | 1 useless lane   | NULL       | `<h1>hello</h1>` | {"e":"monke"} |
| monke    | 2    | 2 car street     | John Doe   | `<p>no</p>`      | NULL          |
| John Doe | 3    | 53428 5th street | NULL       | `<p>L</p>`       | NULL          |

| ID | Group | Content | Reply | Reactions              |
|----|-------|---------|-------|------------------------|
| 1  | 3     | hi      | 2     | {"spikey": "thumbsup"} |
| 2  | 15    | ok      | NULL  | NULL                   |

| ID | Members        | Public |
|----|----------------|--------|
| 1  | e,idot,spikey  | true   |
| 2  | John Doe,loser | false  |

once youre done, in the sqlite3 shell please run `PRAGMA journal_mode=WAL;` in order to allow concurrent writes. you can find more about this at https://www.sqlite.org/wal.html

Set up the CGI server to use the script, and then put websocketd on port 8888 with the script `skyclan.wsd`. An example way to run this would be `websocketd --port=8888 skyclan.wsd `. If you wish to use a different port please edit `htdocs/script.js` to accept connections on another port.
